By uruchomić projekt wystarczy zaimportować go do Android studio i uruchomić.

Aplikacja pokazuje publiczną tablicę:
https://trello.com/b/daQeU9Ts/my-board

Użytkownika oraz tablicę można zmienić za pomocą stałych w klasie Connection.

UWAGA!!!
Przy pierwszym uruchomieniu może pojawić się komunikat o braku klasy początkowego activity.
Błąd należy zignorować. Nie pojawi się on przy kolejnych uruchomieniach.
Ostrzeżenie wynika ze specyfiki biblioteki AndroidAnnotation(https://github.com/excilys/androidannotations/wiki)
która wykorzystuje adnotacje czasu kompilacji.

