package org.bitbucket.kubatatami.trellotest.activities;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.bitbucket.kubatatami.trellotest.R;
import org.bitbucket.kubatatami.trellotest.adapters.TrelloListPageAdapter;
import org.bitbucket.kubatatami.trellotest.connection.Connection;
import org.bitbucket.kubatatami.trellotest.connection.models.Board;
import org.bitbucket.kubatatami.trellotest.connection.models.TrelloCard;
import org.bitbucket.kubatatami.trellotest.connection.models.TrelloList;
import org.bitbucket.kubatatami.trellotest.fragments.TrelloListPageFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

    @ViewById(R.id.list_view_pager)
    protected ViewPager listViewPager;
    @ViewById(R.id.list_loader)
    protected View loaderView;
    @Bean
    protected Connection connection;

    private TrelloListPageAdapter listPageAdapter;
    private Board board;
    private Map<String, List<TrelloCard>> cardMap = new HashMap<>();

    @AfterViews
    protected void initViewPager() {
        listPageAdapter = new TrelloListPageAdapter(getSupportFragmentManager());
        loadCards();
        loadBoard();
        listViewPager.setAdapter(listPageAdapter);
    }

    private void loadBoard() {
        connection.getTrelloAPI().getBoard(Connection.BOARD_ID, new MainCallback<Board>() {
            @Override
            public void success(Board board, Response response) {
                MainActivity.this.board = board;
            }

        });
    }

    public void loadCards() {
        connection.getTrelloAPI().getBoardLists(Connection.BOARD_ID, new MainCallback<List<TrelloList>>() {
            @Override
            public void success(final List<TrelloList> trelloLists, Response response) {
                connection.getTrelloAPI().getCards(Connection.BOARD_ID, new MainCallback<List<TrelloCard>>() {
                    @Override
                    public void success(List<TrelloCard> result, Response response) {
                        cardMap.clear();
                        for (TrelloCard card : result) {
                            List<TrelloCard> trelloCards;
                            if (!cardMap.containsKey(card.getIdList())) {
                                trelloCards = new ArrayList<>();
                                cardMap.put(card.getIdList(), trelloCards);
                            } else {
                                trelloCards = cardMap.get(card.getIdList());
                            }
                            trelloCards.add(card);
                        }
                        listPageAdapter.setTrelloLists(trelloLists);
                        loaderView.setVisibility(View.GONE);
                    }
                });
            }
        });
    }

    public Board getBoard() {
        return board;
    }

    public Map<String, List<TrelloCard>> getCardMap() {
        return cardMap;
    }

    public List<TrelloList> getTrelloLists(){
        return listPageAdapter.getTrelloLists();
    }

    public void reload() {
        for(int i=0;i<listPageAdapter.getCount();i++){
            ((TrelloListPageFragment)getActiveFragment(listViewPager,i)).reload();
        }
    }

    public Fragment getActiveFragment(ViewPager container, int position) {
        String name = makeFragmentName(container.getId(), listPageAdapter.getItemId(position));
        return getSupportFragmentManager().findFragmentByTag(name);
    }

    private static String makeFragmentName(int viewId, long index) {
        return "android:switcher:" + viewId + ":" + index;
    }

    protected abstract class MainCallback<T> implements Callback<T> {

        @Override
        public void failure(RetrofitError error) {
            loaderView.setVisibility(View.GONE);
        }
    }
}
