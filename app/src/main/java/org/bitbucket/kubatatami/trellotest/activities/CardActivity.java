package org.bitbucket.kubatatami.trellotest.activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.OptionsMenuItem;
import org.androidannotations.annotations.ViewById;
import org.bitbucket.kubatatami.trellotest.R;
import org.bitbucket.kubatatami.trellotest.connection.BaseCallback;
import org.bitbucket.kubatatami.trellotest.connection.Connection;
import org.bitbucket.kubatatami.trellotest.connection.models.TrelloCard;
import org.bitbucket.kubatatami.trellotest.connection.models.TrelloList;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.client.Response;

/**
 * Created by Kuba on 12/05/15.
 */
@EActivity(R.layout.activity_card)
@OptionsMenu(R.menu.menu_card_activity)
public class CardActivity extends AppCompatActivity {

    public final static int RESULT_ADD_OR_EDIT = 1;
    public final static int RESULT_DELETE = 2;
    public final static int RESULT_MOVE = 3;

    @Extra
    protected TrelloCard card;
    @Extra
    protected TrelloList trelloList;
    @Extra
    protected ArrayList<TrelloList> trelloLists;

    @ViewById(R.id.card_name)
    protected TextView cardNameView;
    @ViewById(R.id.card_description)
    protected TextView cardDescriptionView;
    @Bean
    protected Connection connection;

    @OptionsMenuItem(R.id.delete_card)
    protected MenuItem deleteCardMenuItem;

    @AfterViews
    protected void init() {
        if (card != null) {
            cardNameView.setText(card.getName());
            cardDescriptionView.setText(card.getDesc());
        } else {
            setTitle(R.string.add_card);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (card != null) {
            setTitle(R.string.card_edit);
        } else {
            setTitle(getString(R.string.create_card) + " - " + trelloList.getName());
            deleteCardMenuItem.setVisible(false);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @OptionsItem(R.id.delete_card)
    protected void deleteClick() {
        connection.getTrelloAPI().deleteCard(card.getId(), Connection.TRELLO_KEY, Connection.TRELLO_TOKEN, new BaseCallback<Void>() {
            @Override
            public void success(Void aVoid, Response response) {
                finish(RESULT_DELETE, card);
            }
        });
    }

    @OptionsItem(R.id.move_card)
    protected void moveClick() {
        CardActivity_.TrelloListDialogFragment_.builder()
                .trelloLists(trelloLists).build().show(getSupportFragmentManager(),"trello_lists");
    }

    @OptionsItem(R.id.done)
    protected void doneClick() {
        Callback<TrelloCard> callback = new BaseCallback<TrelloCard>() {
            @Override
            public void success(TrelloCard trelloCard, Response response) {
                finish(RESULT_ADD_OR_EDIT, trelloCard);
            }
        };
        if (card != null) {
            connection.getTrelloAPI().editCard(
                    card.getId(),
                    cardNameView.getText().toString(),
                    cardDescriptionView.getText().toString(),
                    Connection.TRELLO_KEY, Connection.TRELLO_TOKEN, callback);
        } else {
            connection.getTrelloAPI().addCard(
                    cardNameView.getText().toString(),
                    cardDescriptionView.getText().toString(),
                    trelloList.getId(),
                    Connection.TRELLO_KEY, Connection.TRELLO_TOKEN, callback);
        }
    }

    protected void finish(int result, TrelloCard card) {
        Intent intent = new Intent();
        intent.putExtra("trelloCard", card);
        setResult(result, intent);
        finish();
    }

    public void moveCard(final String listId){
        connection.getTrelloAPI().moveCard(card.getId(), listId, Connection.TRELLO_KEY, Connection.TRELLO_TOKEN, new BaseCallback<Void>() {
            @Override
            public void success(Void ignored, Response response) {
                Intent intent = new Intent();
                intent.putExtra("listId", listId);
                intent.putExtra("trelloCard", card);
                setResult(RESULT_MOVE, intent);
                finish();
            }
        });
    }

    @EFragment
    public static class TrelloListDialogFragment extends DialogFragment{

        @FragmentArg
        protected ArrayList<TrelloList> trelloLists;


        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.move_to);
            builder.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.select_dialog_item, trelloLists),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            ((CardActivity) getActivity()).moveCard(trelloLists.get(item).getId());
                        }
                    });
            return builder.create();
        }
    }
}
