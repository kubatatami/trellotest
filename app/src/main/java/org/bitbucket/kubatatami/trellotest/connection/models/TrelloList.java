package org.bitbucket.kubatatami.trellotest.connection.models;

import java.io.Serializable;

public class TrelloList implements Serializable {

    private static final long serialVersionUID = -2136697270778400560L;

    private String id;

    private String name;

    private Boolean closed;

    private String pos;

    private String idBoard;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getClosed() {
        return closed;
    }

    public void setClosed(Boolean closed) {
        this.closed = closed;
    }


    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public String getIdBoard() {
        return idBoard;
    }

    public void setIdBoard(String idBoard) {
        this.idBoard = idBoard;
    }

    @Override
    public String toString() {
        return name;
    }
}