package org.bitbucket.kubatatami.trellotest.fragments;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.draggable.ItemDraggableRange;
import com.h6ah4i.android.widget.advrecyclerview.draggable.RecyclerViewDragDropManager;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractDraggableItemViewHolder;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;
import org.bitbucket.kubatatami.trellotest.R;
import org.bitbucket.kubatatami.trellotest.activities.CardActivity;
import org.bitbucket.kubatatami.trellotest.activities.CardActivity_;
import org.bitbucket.kubatatami.trellotest.activities.MainActivity;
import org.bitbucket.kubatatami.trellotest.connection.BaseCallback;
import org.bitbucket.kubatatami.trellotest.connection.Connection;
import org.bitbucket.kubatatami.trellotest.connection.models.Board;
import org.bitbucket.kubatatami.trellotest.connection.models.TrelloCard;
import org.bitbucket.kubatatami.trellotest.connection.models.TrelloList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Kuba on 12/05/15.
 */
@OptionsMenu(R.menu.menu_trello_list_page_fragment)
@EFragment(R.layout.fragment_trello_list_page)
public class TrelloListPageFragment extends Fragment {

    protected final int CREATE_OR_EDIT_REQUEST = 1;

    @Bean
    protected Connection connection;

    @FragmentArg
    protected TrelloList trelloList;

    @ViewById(R.id.trello_list_recycler_view)
    protected RecyclerView recyclerView;

    protected class CardAdapter extends RecyclerView.Adapter<Holder> implements DraggableItemAdapter<Holder> {

        public CardAdapter() {
            setHasStableIds(true);
        }

        @Override
        public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_trello_card, parent, false);
            return new Holder(view);
        }

        @Override
        public void onBindViewHolder(Holder holder, int position) {
            final TrelloCard card = getCardList().get(position);
            holder.nameView.setText(card.getName());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ArrayList<TrelloList> trelloLists = new ArrayList<>(getMainActivity().getTrelloLists());
                    trelloLists.remove(trelloList);
                    CardActivity_.intent(TrelloListPageFragment.this).card(card)
                            .trelloLists(trelloLists)
                            .startForResult(CREATE_OR_EDIT_REQUEST);
                }
            });
        }

        @Override
        public int getItemCount() {
            return getCardList().size();
        }

        @Override
        public long getItemId(int position) {
            return getCardList().get(position).getId().hashCode();
        }

        @Override
        public void onMoveItem(int fromPosition, int toPosition) {
            if (fromPosition == toPosition) {
                return;
            }
            int newPos = 0;
            if (toPosition > 0) {
                newPos += getCardList().get(toPosition - 1).getPosTop();
            }
            if (toPosition < getItemCount() - 1) {
                newPos += getCardList().get(toPosition + 1).getPosTop();
            } else {
                newPos += getCardList().get(toPosition).getPosTop() * 2;
            }

            TrelloCard card = getCardList().remove(fromPosition);
            getCardList().add(toPosition, card);

            connection.getTrelloAPI().moveCardPosition(card.getId(), newPos / 2,
                    Connection.TRELLO_KEY, Connection.TRELLO_TOKEN, new BaseCallback<Void>());
            notifyItemMoved(fromPosition, toPosition);
        }

        @Override
        public boolean onCheckCanStartDrag(Holder holder, int position, int x, int y) {
            return true;
        }

        @Override
        public ItemDraggableRange onGetItemDraggableRange(Holder holder, int position) {
            // no drag-sortable range specified
            return null;
        }
    }

    @AfterViews
    protected void init() {
        Collections.sort(getCardList());
        RecyclerViewDragDropManager dragDropManager = new RecyclerViewDragDropManager();
        RecyclerView.Adapter adapter = dragDropManager.createWrappedAdapter(new CardAdapter());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
        dragDropManager.attachRecyclerView(recyclerView);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        Board board = getMainActivity().getBoard();
        if (board != null) {
            getMainActivity().setTitle(board.getName() + " - " + trelloList.getName());
        }
    }

    @OnActivityResult(CREATE_OR_EDIT_REQUEST)
    protected void onCreateCard(int result, Intent data) {
        if (result == CardActivity.RESULT_ADD_OR_EDIT) {
            TrelloCard card = (TrelloCard) data.getSerializableExtra("trelloCard");
            if(getCardList().contains(card)){
                int index = getCardList().indexOf(card);
                getCardList().remove(index);
                getCardList().add(index, card);
            }else{
                getCardList().add(card);
            }
            recyclerView.getAdapter().notifyDataSetChanged();
        } else if (result == CardActivity.RESULT_DELETE) {
            TrelloCard card = (TrelloCard) data.getSerializableExtra("trelloCard");
            getCardList().remove(card);
            recyclerView.getAdapter().notifyDataSetChanged();
        }else if (result == CardActivity.RESULT_MOVE) {
            TrelloCard card = (TrelloCard) data.getSerializableExtra("trelloCard");
            String listId = data.getStringExtra("listId");
            Map<String,List<TrelloCard>> trelloListMap = getMainActivity().getCardMap();
            trelloListMap.get(listId).add(card);
            trelloListMap.get(card.getIdList()).remove(card);
            card.setIdList(listId);
            getMainActivity().reload();
        }
    }

    public void reload(){
        recyclerView.getAdapter().notifyDataSetChanged();
    }

    @OptionsItem(R.id.add_card)
    protected void addClick() {
        CardActivity_.intent(this).trelloList(trelloList).startForResult(CREATE_OR_EDIT_REQUEST);
    }

    private List<TrelloCard> getCardList() {
        return getMainActivity().getCardMap().get(trelloList.getId());
    }


    private MainActivity getMainActivity() {
        return (MainActivity) getActivity();
    }

    private static class Holder extends AbstractDraggableItemViewHolder {

        TextView nameView;

        public Holder(View itemView) {
            super(itemView);
            nameView = (TextView) itemView.findViewById(R.id.item_trello_name);
        }
    }
}
