package org.bitbucket.kubatatami.trellotest.connection;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Kuba on 12/05/15.
 */
public class BaseCallback<T> implements Callback<T> {
    @Override
    public void success(T t, Response response) {

    }

    @Override
    public void failure(RetrofitError error) {

    }
}
