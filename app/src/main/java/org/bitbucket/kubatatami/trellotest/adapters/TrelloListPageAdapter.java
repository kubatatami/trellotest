package org.bitbucket.kubatatami.trellotest.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import org.bitbucket.kubatatami.trellotest.connection.models.TrelloList;
import org.bitbucket.kubatatami.trellotest.fragments.TrelloListPageFragment_;

import java.util.List;

/**
 * Created by Kuba on 12/05/15.
 */
public class TrelloListPageAdapter extends FragmentPagerAdapter {

    private List<TrelloList> trelloLists;

    public TrelloListPageAdapter(FragmentManager fm) {
        super(fm);
    }

    public void setTrelloLists(List<TrelloList> trelloLists) {
        this.trelloLists = trelloLists;
        notifyDataSetChanged();
    }

    public List<TrelloList> getTrelloLists() {
        return trelloLists;
    }

    @Override
    public Fragment getItem(int position) {
        return TrelloListPageFragment_.builder().trelloList(trelloLists.get(position)).build();
    }

    @Override
    public int getCount() {
        return trelloLists == null ? 0 : trelloLists.size();
    }
}
