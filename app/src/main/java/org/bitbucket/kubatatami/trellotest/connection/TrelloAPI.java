package org.bitbucket.kubatatami.trellotest.connection;

import org.bitbucket.kubatatami.trellotest.connection.models.Board;
import org.bitbucket.kubatatami.trellotest.connection.models.TrelloCard;
import org.bitbucket.kubatatami.trellotest.connection.models.TrelloList;

import java.util.List;

import retrofit.Callback;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;

public interface TrelloAPI {

    @GET("/boards/{id}")
    void getBoard(@Path("id") String id, Callback<Board> callback);

    @GET("/boards/{id}/lists")
    void getBoardLists(@Path("id") String id, Callback<List<TrelloList>> callback);

    @GET("/boards/{id}/cards")
    void getCards(@Path("id") String id, Callback<List<TrelloCard>> callback);

    @POST("/cards")
    void addCard(@Query("name") String name, @Query("desc") String description,
                 @Query("idList") String idList, @Query("key") String key,
                 @Query("token") String token, Callback<TrelloCard> callback);

    @PUT("/cards/{id}/")
    void editCard(@Path("id") String id,
                  @Query("name") String name,
                  @Query("desc") String description,
                  @Query("key") String key,
                  @Query("token") String token, Callback<TrelloCard> callback);

    @DELETE("/cards/{id}/")
    void deleteCard(@Path("id") String id,
                    @Query("key") String key,
                    @Query("token") String token, Callback<Void> callback);

    @PUT("/cards/{id}/")
    void moveCardPosition(@Path("id") String id,
                          @Query("pos") int pos,
                          @Query("key") String key,
                          @Query("token") String token, Callback<Void> callback);


    @PUT("/cards/{id}/")
    void moveCard(@Path("id") String id,
                  @Query("idList") String idList,
                  @Query("key") String key,
                  @Query("token") String token, Callback<Void> callback);

}