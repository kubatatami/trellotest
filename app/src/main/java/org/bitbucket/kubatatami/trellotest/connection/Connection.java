package org.bitbucket.kubatatami.trellotest.connection;

import android.content.Context;
import android.widget.Toast;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.bitbucket.kubatatami.trellotest.R;

import retrofit.ErrorHandler;
import retrofit.RestAdapter;
import retrofit.RetrofitError;

/**
 * Created by Kuba on 12/05/15.
 */
@EBean(scope = EBean.Scope.Singleton)
public class Connection {

    @RootContext
    protected Context context;

    private static final String TRELLO_URL = "https://api.trello.com/1/";
    public static final String BOARD_ID = "daQeU9Ts";
    public static final String TRELLO_KEY = "50a7a4f53de4a61f8a207611161578e8";
    public static final String TRELLO_TOKEN = "054ea705d896265b73b4a0a6102037ae87a7b6976ccc791daaf9121bd6e0297f";

    private TrelloAPI trelloAPI;

    @AfterInject
    protected void init() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(TRELLO_URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setErrorHandler(new ErrorHandler() {
                    @Override
                    public Throwable handleError(RetrofitError cause) {
                        if (cause.getKind().equals(RetrofitError.Kind.NETWORK)) {
                            Toast.makeText(context, R.string.connection_error, Toast.LENGTH_SHORT).show();
                        } else if (cause.getKind().equals(RetrofitError.Kind.NETWORK)) {
                            Toast.makeText(context, R.string.server_error, Toast.LENGTH_SHORT).show();
                        }
                        return cause;
                    }
                })
                .build();
        trelloAPI = restAdapter.create(TrelloAPI.class);
    }


    public TrelloAPI getTrelloAPI() {
        return trelloAPI;
    }
}
